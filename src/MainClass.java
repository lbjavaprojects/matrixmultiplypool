import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainClass {
	private static final int ROW_COUNT=80;
	private static final int COL_COUNT=80;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[][] A=new double[ROW_COUNT][COL_COUNT];
		double[][] B=new double[ROW_COUNT][COL_COUNT];
		double[][] C=new double[ROW_COUNT][COL_COUNT];
		for(int i=0;i<ROW_COUNT;i++){
			for(int j=0;j<COL_COUNT;j++){
				A[i][j]=1;
				B[i][j]=1;
				C[i][j]=0;
			}
		}
		System.out.println("Pocz�tkowy stan macierzy:");
		System.out.println("A: "+Arrays.deepToString(A));
		System.out.println("B: "+Arrays.deepToString(B));
		System.out.println("C: "+Arrays.deepToString(C));
		long startTime=System.currentTimeMillis();
		
		ExecutorService pool=Executors.newFixedThreadPool(3);
		for(int i=0;i<ROW_COUNT;i++){
			for(int j=0;j<COL_COUNT;j++){
				pool.execute(new MatrixMultiplier(i, j, A, B, C));
			}
		}
		pool.shutdown();
		try {
			if(pool.awaitTermination(1, TimeUnit.SECONDS))
			{
				long stopTime=System.currentTimeMillis();
				System.out.println("Ko�cowy stan macierzy po "+(stopTime-startTime)+ ": milisekundach");
				System.out.println("C: "+Arrays.deepToString(C));
			}
			else{
				System.out.println("Zadanie nie wykona�y si� przed up�yni�ciem timeoutu.");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
